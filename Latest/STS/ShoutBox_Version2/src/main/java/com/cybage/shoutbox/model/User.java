package com.cybage.shoutbox.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.cybage.shoutbox.model.Shout;
import com.cybage.shoutbox.model.Friend;

@Entity
public class User {

	private int userId;
	private String firstName, lastName, password, status, userName, email, role, gender;
	private List<Shout> shouts = new ArrayList<Shout>();
	private String profilePic, profilePicPath;

	private List<Friend> friends = new ArrayList<Friend>();

	public User() {
		super();
	}

	// Constructor with Id parameter
	public User(int userId, String firstName, String lastName, String password, String status, String userName,
			String email, String role, String gender, List<Shout> shouts, String profilePic, String profilePicPath,
			List<Friend> friends) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.status = status;
		this.userName = userName;
		this.email = email;
		this.role = role;
		this.gender = gender;
		this.shouts = shouts;
		this.profilePic = profilePic;
		this.profilePicPath = profilePicPath;
		this.friends = friends;
	}

	// Constructor without Id parameter
	public User(String firstName, String lastName, String password, String status, String userName, String email,
			String role, String gender, List<Shout> shouts, String profilePic, String profilePicPath,
			List<Friend> friends) {

		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.status = status;
		this.userName = userName;
		this.email = email;
		this.role = role;
		this.gender = gender;
		this.shouts = shouts;
		this.profilePic = profilePic;
		this.profilePicPath = profilePicPath;
		this.friends = friends;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	// One to many mapping with shout table
	@OneToMany(mappedBy = "myUser", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public List<Shout> getShouts() {
		return shouts;
	}

	public void setShouts(List<Shout> shouts) {
		this.shouts = shouts;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getProfilePicPath() {
		return profilePicPath;
	}

	public void setProfilePicPath(String profilePicPath) {
		this.profilePicPath = profilePicPath;
	}

	@OneToMany(mappedBy = "senderId", cascade = CascadeType.ALL)
	public List<Friend> getFriends() {
		return friends;
	}

	public void setFriends(List<Friend> friends) {
		this.friends = friends;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", password="
				+ password + ", status=" + status + ", userName=" + userName + ", email=" + email + ", role=" + role
				+ ", gender=" + gender + ", shouts=" + shouts + ", profilePic=" + profilePic + ", profilePicPath="
				+ profilePicPath + "]";
	}
}
