package com.cybage.shoutbox.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cybage.shoutbox.model.User;

public interface UserRepo extends JpaRepository<User, Integer> {

}
