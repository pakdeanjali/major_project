package com.cybage.shoutbox.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Shout {

	private int shoutId;
	private User myUser;
	private String message, status;
	private String image;
	private String imagePath;
	private Date timeStamp;
	private int report;

	private List<Comment> comments = new ArrayList<Comment>();

	public Shout() {
		super();
	}

	// Constructor with Id parameter
	public Shout(int shoutId, User myUser, String message, String status, String image, String imagePath,
			Date timeStamp, int report, List<Comment> comments) {
		super();
		this.shoutId = shoutId;
		this.myUser = myUser;
		this.message = message;
		this.status = status;
		this.image = image;
		this.imagePath = imagePath;
		this.timeStamp = timeStamp;
		this.report = report;
		this.comments = comments;
	}

	// Constructor without Id parameter
	public Shout(User myUser, String message, String status, String image, String imagePath, Date timeStamp, int report,
			List<Comment> comments) {
		super();

		this.myUser = myUser;
		this.message = message;
		this.status = status;
		this.image = image;
		this.imagePath = imagePath;
		this.timeStamp = timeStamp;
		this.report = report;
		this.comments = comments;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getShoutId() {
		return shoutId;
	}

	public void setShoutId(int shoutId) {
		this.shoutId = shoutId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public int getReport() {
		return report;
	}

	public void setReport(int report) {
		this.report = report;
	}

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	@ManyToOne
	@JoinColumn(name = "uID")
	public User getMyUser() {
		return myUser;
	}

	public void setMyUser(User myUser) {
		this.myUser = myUser;
	}

	// One to many mapping with comment table
	@OneToMany(mappedBy = "thisShout", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Shout [shoutId=" + shoutId + ", myUser=" + myUser + ", message=" + message + ", status=" + status
				+ ", image=" + image + ", imagePath=" + imagePath + ", timeStamp=" + timeStamp + ", report=" + report
				+ ", comments=" + comments + "]";
	}

}
