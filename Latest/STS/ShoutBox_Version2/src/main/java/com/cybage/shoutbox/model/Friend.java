package com.cybage.shoutbox.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

 @Entity
public class Friend {
	
	private int friendShipId;
	private User senderId;
	private int recieverId;
	private String status;
	
	public Friend() {
		super();
	}

	public Friend(int friendShipId, User senderId, int recieverId, String status) {
		super();
		this.friendShipId = friendShipId;
		this.senderId = senderId;
		this.recieverId = recieverId;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="friendshipId")
	public int getFriendShipId() {
		return friendShipId;
	}

	public void setFriendShipId(int friendId) {
		this.friendShipId = friendId;
	}

	
	@ManyToOne
	@JoinColumn(name = "senderId")
	public User getSenderId() {
		return senderId;
	}

	public void setSenderId(User senderId) {
		this.senderId = senderId;
	}

	public int getRecieverId() {
		return recieverId;
	}

	public void setRecieverId(int recieverId) {
		this.recieverId = recieverId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Friend [friendShipId=" + friendShipId + ", senderId=" + senderId + ", recieverId=" + recieverId + ", status="
				+ status + "]";
	}
	
}
