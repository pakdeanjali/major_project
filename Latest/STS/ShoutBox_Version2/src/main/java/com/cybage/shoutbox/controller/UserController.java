 package com.cybage.shoutbox.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.shoutbox.model.User;
import com.cybage.shoutbox.repo.UserRepo;

@RestController
@RequestMapping("users")
@CrossOrigin(origins="http://localhost:3001")
public class UserController {

	@Autowired
	private UserRepo userRepo;

	@GetMapping(produces="application/json")
	public List<User> getAllUsers() {

		return userRepo.findAll();
	}

	@PostMapping
	public String addUser(@RequestBody User user) {
		userRepo.save(user);
		return "User Added";
	}

	@GetMapping("/{userId}")
	public User getUserById(@PathVariable("userId") int userId) {
		return userRepo.findOne(userId);
	}

	@DeleteMapping("/{userId}")
	public String deleteUserById(@PathVariable("userId") int userId) {
		userRepo.delete(userId);
		return "User Deleted";
	}

	@PutMapping
	public String updateUser(@RequestBody User b) {
		userRepo.save(b);
		return "User updated successfully";

	}

}
