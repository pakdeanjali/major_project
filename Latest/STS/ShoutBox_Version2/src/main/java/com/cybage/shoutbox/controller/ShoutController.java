package com.cybage.shoutbox.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.shoutbox.model.Shout;
import com.cybage.shoutbox.repo.ShoutRepo;

@RestController
@RequestMapping("shouts")
public class ShoutController {

	@Autowired
	private  ShoutRepo shoutRepo;

	@GetMapping
	public List<Shout> getAllShouts() {

		return shoutRepo.findAll();
	}

	@PostMapping
	public String addShout(@RequestBody Shout shout) {
		shoutRepo.save(shout);
		return "Shout Added";
	}
	
	@GetMapping("/{shoutId}")
	public Shout getShoutById(@PathVariable("shoutId") int shoutId) {
		return shoutRepo.findOne(shoutId);
	}

	@DeleteMapping("/{shoutId}")
	public String deleteShoutById(@PathVariable("shoutId") int shoutId) {
		shoutRepo.delete(shoutId);
		return "Shout Deleted";
	}
	
	
}
