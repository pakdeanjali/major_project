import React, { Component } from 'react';
import axios from 'axios';

class DeleteBook extends Component {

    constructor(props) {
        super(props);
        
        this.state = {

             id : "",
             message : ""
        }
    }
    
    inputHandler(event){

        this.setState({
                [event.target.name] : event.target.value
        });
    }

    deleteHandler(event){

         event.preventDefault();
         const url = `http://localhost:8080/books/${this.state.id}`;
         axios.delete(url)
         .then(response => {
              
               console.log(response);
               alert(`Book Deleted Successfully with id ${this.state.id}`)
               this.setState({message : "Book Found and Deleted .."});
         })
         .catch(error => {

              console.log(error);
              this.setState({message : "Book Not Found !!!"});
         });

         
    }


    render() {

        const {id,message} = this.state;

        return (
            <div>
                <h1>Delete Book</h1>
                <form onSubmit={e => this.deleteHandler(e)}>

                    
                    <div>
                        Enter the Book Id <input type="text" name="id" value={id} onChange={ (e) => this.inputHandler(e) }/>
                        <input type ="submit" value="Delete Book"/>          
                    </div>
                    <br/><br/>
                    <div>
                    {message}
                    </div>

                </form>
            </div>
        );
    }
}

export default DeleteBook;