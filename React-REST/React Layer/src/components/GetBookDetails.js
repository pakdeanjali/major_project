import React, { Component } from 'react';
import axios  from 'axios';

class GetBookDetails extends Component {

    constructor(props) {
        super(props);
        
        this.state = {

              book : null
        }
    }
    

    componentDidMount(){

        const id = parseInt(this.props.match.params.id);
        
        const url = `http://localhost:8080/books/${id}`;

        
         axios.get(url)
         .then(response =>{

               console.log(response.data);
               this.setState({book : response.data});
         })
         .catch(error => {

                console.log(error);
         })
    }




    render() {

        const {book} = this.state;

        return (
            <div>
                 <h1>Book Details</h1>

                 {book ? 
                 <table cellSpacing="20px" cellPadding="10px">
                       <tr>
                           <th>Book Id</th>
                           <th>Book Name</th>
                           <th>Book Price</th>
                      </tr>
                   
                
                                    
                    <tr>
                      <td>{book.id}</td>
                      <td>{book.name}</td>
                      <td>{book.price}</td>
                     </tr>
                                   
                                  
            
                   </table>  
                 :null}
            </div>
        );
    }
}

export default GetBookDetails;