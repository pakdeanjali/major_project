import React, { Component } from 'react';
import axios from 'axios';

class AddBook extends Component {

    constructor(props) {
        super(props);
        
        this.state = {

                name : "",
                price : ""
        }

        
    }
    

    inputHandler(event){

        this.setState({[event.target.name] : event.target.value});
    }

    sumbitHandler(event){

        event.preventDefault();
        this.postBookDetails();
        alert("Book Added Sucessfully");
    }

    postBookDetails(){

         axios.post("http://localhost:8080/books",this.state)
         .then(response => {
             console.log(response);
         })
         .catch(error => {

              console.log(error);
         })
    }

    render() {

        const {name,price} = this.state;

        return (
            <div>
                     <h1>Add Book</h1>
                     <form onSubmit={ (e) => this.sumbitHandler(e)}>
                         <div>
                         Enter the Book Name <input type="text" name ="name" value={name} onChange={ (e) => this.inputHandler(e)}/>
                         </div>
                         <div>
                         Enter the Book Price <input type="text" name ="price" value={price} onChange={ (e) => this.inputHandler(e)}/>
                         </div>
                         <div>
                         <input type="submit" value="Add Book"/>
                         </div>
                    </form>                  
            </div>
        );
    }
}

export default AddBook;