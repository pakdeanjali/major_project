import React, { Component } from 'react';
import axios  from 'axios';
import {Link} from 'react-router-dom';


class BookList extends Component {

    constructor(props) {
        super(props);
        
        this.state = {

                bookList : []
        }
    }

    componentDidMount(){

          this.fetchBooks();
    }

    fetchBooks(){

         axios.get("http://localhost:8080/books")
         .then(response => {

                console.log(response);
                this.setState({bookList : response.data});
         })
         .catch(error => {

                console.log(error);
         });
    }


    clickHandler(event,bookId){

            event.preventDefault();
            this.props.history.push(`/getBookDetails/${bookId}`);
    }

    

    render() {

        const {bookList} = this.state;
        
        return (
            <div>
                   <h1>Book List</h1>
                   <table cellSpacing="20px" cellPadding="10px">
                       <tr>
                           <th>Book Id</th>
                           <th>Book Name</th>
                      </tr>
                   {
                
                       bookList.map(
                                    
                           book => 
                                    
                                    <tr key={book.id}>
                                     <td><Link onClick={(e) => this.clickHandler(e,book.id)}>{book.id}</Link> </td>
                                     <td>{book.name}</td>
                                    </tr>
                                   
                                  
                   )}
                   </table>   


            </div>
        );
    }
}

export default BookList;