import React from 'react';
import './App.css';
import BookList from './components/BookList';
import AddBook from './components/AddBook';
import DeleteBook from './components/DeleteBook';

import {BrowserRouter,Link,Switch,Route} from 'react-router-dom';
import GetBookDetails from './components/GetBookDetails';

function App() {
  return (
    <BrowserRouter>
    <div> 
      <center>
      <header className="heading1">
      <h1>Book Library Management System</h1>
      </header>
    <div className="sidebar">
  <Link className="active" to="/">See All Books In Library</Link>
  <Link to="/addBook">Add Book In Library</Link>
  <Link to="/deleteBook">Delete Book From Library</Link>
  </div>

<div className="content">
  <Switch>
  <Route path="/addBook" component={AddBook}/>
  <Route path="/deleteBook" component={DeleteBook}/>
  <Route path="/getBookDetails/:id" component={GetBookDetails}/>
  <Route path="/" component={BookList}/>
  
  </Switch>
</div>
</center>
</div>
</BrowserRouter>
  );
}

export default App;
