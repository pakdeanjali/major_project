package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.example.demo.model.Book;

@RestResource(path="books",rel="books")
public interface BookRepository extends JpaRepository<Book, Integer>
{

}
