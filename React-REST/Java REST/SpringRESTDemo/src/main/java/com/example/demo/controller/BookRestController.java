package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Book;
import com.example.demo.repo.BookRepository;

@RestController
@RequestMapping("books")
@CrossOrigin(origins="http://localhost:3000") // This Service Can Be Consumed By React Client.
public class BookRestController 
{
	@Autowired
	private BookRepository repo;
	
	
	@GetMapping(produces="application/json") // This is the MediaType attribute produces json data
	public List<Book> getAllBooks()
	{
		return repo.findAll();
	}
	
	@GetMapping("/{id}")
	public Book getBookById(@PathVariable("id") int id)
	{
		return repo.findOne(id);
	}
	
	@PostMapping
	public Book storeBook(@RequestBody Book book)
	{
		repo.save(book);
		return book;
	}
	
	@DeleteMapping("/{id}")
	public void deleteBook(@PathVariable("id") int id)
	{
		repo.delete(id);
	}
	
	@PutMapping
	public Book updateBook(@RequestBody Book book)
	{
		repo.save(book);
		return book;
	}
	
}
