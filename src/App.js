import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AddUser from './components/addUser';
import HomePage from './components/HomePage'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <div>
          <center>
            <div className="content">
              <Switch>
                <Route path="/addUser" component={AddUser} />
                {/* <Route path="/login" component={Login} /> */}
                {/* <Route path="/getUserDetails/:id" component={GetUserDetails} /> */}
                <Route path="/" component={HomePage} />

              </Switch>
            </div>
          </center>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
