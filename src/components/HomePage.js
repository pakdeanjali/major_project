import React from 'react';
import { BrowserRouter, Link } from 'react-router-dom';

function HomePage() {
    return (
        <BrowserRouter>
            <center>
                <header className="heading1">
                    <h1>Header</h1>
                </header>
                <div className="sidebar">
                    <Link to="/addUser">Register</Link><br />
                    <Link to="/login">Login</Link><br />
                </div>
            </center>
        </BrowserRouter>
    );
}

export default HomePage;