import React from 'react';
import axios from 'axios';

class AddUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            password: "",
            role: "user",
            status: "pending"
        }
    }

    inputHandler(event){

        this.setState({[event.target.name] : event.target.value});
    }

    sumbitHandler(event){

        event.preventDefault();
        this.addUser();
        alert("Registration Sucessful");
    }

    addUser(){

         axios.post("http://localhost:8080/users",this.state)
         .then(response => {
             console.log(response);
         })
         .catch(error => {

              console.log(error);
         })
    }

    render() {

        const {firstname,lastname,email,password} = this.state;

        return (
            <div>
                <h1>Register</h1>
                <form onSubmit={(e) => this.sumbitHandler(e)}>
                    <table>
                        <tr>
                            <td>Enter first name</td>
                            <td><input type="text" name="firstname" value={firstname} onChange={(e) => this.inputHandler(e)} /></td>
                        </tr>

                        <tr>
                            <td>Enter last name</td>
                            <td><input type="text" name="lastname" value={lastname} onChange={(e) => this.inputHandler(e)} /></td>
                        </tr>

                        <tr>
                            <td>Enter email</td>
                            <td><input type="text" name="email" value={email} onChange={(e) => this.inputHandler(e)} /></td>
                        </tr>

                        <tr>
                            <td>Enter password</td>
                            <td><input type="password" name="password" value={password} onChange={(e) => this.inputHandler(e)} /></td>
                        </tr>

                        <tr>
                            <td colspan="2"><input type="submit" value="Register" /></td>
                        </tr>
                    </table>
                </form>
            </div>
        );
    }
}

export default AddUser;