import React from 'react';
import axios from 'axios';
// import { Link } from 'react-router-dom';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        }
    }

    inputHandler(event){

        this.setState({[event.target.name] : event.target.value});
    }

    sumbitHandler(event){

        event.preventDefault();
        this.login();
        // alert("Registration Sucessful");
    }

    addUser(){

         axios.post("http://localhost:8080/users",this.state)
         .then(response => {
             console.log(response);
         })
         .catch(error => {

              console.log(error);
         })
    }

    render() {

        const {username,password} = this.state;

        return (
            <div>
                <h1>Register</h1>
                <form onSubmit={(e) => this.sumbitHandler(e)}>
                    <table>
                        <tr>
                            <td>Username: </td>
                            <td><input type="text" name="username" value={username} onChange={(e) => this.inputHandler(e)} /></td>
                        </tr>

                        <tr>
                            <td>Password: </td>
                            <td><input type="password" name="password" value={password} onChange={(e) => this.inputHandler(e)} /></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td><input type="submit" value="Login" /></td>
                        </tr>
                    </table>
                </form>
            </div>
        );
    }
}

export default Login;