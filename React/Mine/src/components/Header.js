import React from 'react';
import shoutLogo from '../images/shoutlogo.png';
import { withRouter } from 'react-router';

// const goToRegister = props => props.history.push("/addUser");

// const goToLogin = props => props.history.push("/login");

const Header = props => {

    const goToLogin = () => {
        props.history.push("/login");
    };

    const goToRegister = () => {
        props.history.push("/addUser");
    };


    return (
        <div className="header">
            <div className="header-left">
                <img src={shoutLogo} className="logo" alt="Logo" />
            </div>
            <div className="header-right">
                <button className="btn btn-light" onClick={goToRegister}>Register</button>&nbsp; &nbsp;
                <button className="btn btn-light" onClick={goToLogin}>Login</button>
            </div>
        </div>
    );
}

export default withRouter(Header);