import React from 'react';
// import { Link, Switch } from 'react-router-dom';
import Header from './Header';
// import '../styles/mystyles.css';
import HomeImage from '../images/shout.png';
import MyStyle from '../styles/mystyles.css';

class HomePage extends React.Component {

    render() {
        return (
            <div style={{MyStyle}}>
                <Header />
                <div className="main-content">
                    <img src={HomeImage} alt="home" />
                </div>
            </div>
        );
    }

}

export default HomePage;