package com.cybage.shoutbox.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cybage.shoutbox.model.Shout;

public interface ShoutRepo extends JpaRepository<Shout, Integer> {

}
