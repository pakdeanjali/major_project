package com.cybage.shoutbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoutBoxVersion2Application {

	public static void main(String[] args) {
		SpringApplication.run(ShoutBoxVersion2Application.class, args);
	}
}