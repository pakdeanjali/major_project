package com.cybage.shoutbox.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Comment {

	private int cId;
	private String text;
	private Date timeStamp;
	private Shout thisShout;
	//private User userId;
	private int userId;
	
	public Comment() {
		super();
	}

	public Comment(int cId, String text, Date timeStamp, Shout thisShout, int userId) {
		super();
		this.cId = cId;
		this.text = text;
		this.timeStamp = timeStamp;
		this.thisShout = thisShout;
		this.userId = userId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@CreationTimestamp 
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	
	@ManyToOne
	@JoinColumn(name="shoutId")
	public Shout getThisShout() {
		return thisShout;
	}

	public void setThisShout(Shout thisShout) {
		this.thisShout = thisShout;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	@Override
	public String toString() {
		return "Comment [cId=" + cId + ", text=" + text + ", timeStamp=" + timeStamp + ", thisShout=" + thisShout
				+ ", userId=" + userId + "]";
	}

	
	
}
